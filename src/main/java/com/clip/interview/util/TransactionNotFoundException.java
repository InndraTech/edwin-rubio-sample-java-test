package com.clip.interview.util;

public class TransactionNotFoundException extends Exception {

    public TransactionNotFoundException(String message) {
        super(message);
    }
}
