package com.clip.interview.controller.impl;

import com.clip.interview.controller.TransactionController;
import com.clip.interview.mapper.TransactionDto;
import com.clip.interview.mapper.TransactionSumDto;
import com.clip.interview.model.Transaction;
import com.clip.interview.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.util.List;

@RestController
public class TransactionControllerImpl implements TransactionController {

    @Autowired
    TransactionService transactionService;

    @Override
    public TransactionDto add(TransactionDto transactionDto) {
        return transactionService.add(transactionDto);
    }

    @Override
    public Response findByUserIdAndTransactionId(String transactionId, Long userId) {
        return transactionService.findByUserIdAndTransactionId(transactionId, userId);
    }

    @Override
    public List<TransactionDto> findAllByUserId(Long userId) {
        return transactionService.findAllByUserId(userId);
    }

    @Override
    public TransactionSumDto sum(Long userId) {
        return transactionService.sum(userId);
    }

    @Override
    public TransactionDto random() {
        return transactionService.random();
    }
}
