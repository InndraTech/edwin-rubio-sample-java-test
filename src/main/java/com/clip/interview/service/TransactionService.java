package com.clip.interview.service;

import com.clip.interview.mapper.TransactionDto;
import com.clip.interview.mapper.TransactionSumDto;
import com.clip.interview.model.Transaction;

import javax.ws.rs.core.Response;
import java.util.List;

public interface TransactionService {

    TransactionDto add(TransactionDto transactionDto);

    Response findByUserIdAndTransactionId(String transactionId, Long userId) ;

    List<TransactionDto> findAllByUserId(Long userId);

    TransactionSumDto sum(Long userId);

    TransactionDto random();

}
